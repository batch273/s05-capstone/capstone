class Customer {
  constructor(email) {
    // Initialize a new customer with an email, an empty cart, and an empty order list
    this.email = email;
    this.cart = new Cart();
    this.orders = [];
  }

  // Checkout the contents of the cart
  checkOut() {
    if (this.cart.isEmpty()) {
      console.log('Cart is empty');
      return;
    }

    // Compute the total amount of the cart
    this.cart.computeTotal();

    // Create an order object and add it to the customer's order list
    const order = {
      products: this.cart.getContents(),
      totalAmount: this.cart.totalAmount
    };
    this.orders.push(order);

    // Clear the cart and display the order
    this.cart.clearCartContents();
    console.log('Order placed:', order);
  }
}

// Define the Cart class
class Cart {
  constructor() {
    // Initialize a new cart with an empty contents list and a total amount of 0
    this.contents = [];
    this.totalAmount = 0;
  }

  // Add a product to the cart
  addToCart(product, quantity) {
    this.contents.push({ product, quantity });
    
    return this
  }

  // Display the contents of the cart
  showCartContents() {
    console.log('Cart contents:', this.contents);
  }

  // Update the quantity of a product in the cart
  updateProductQuantity(name, newQuantity) {
    const productIndex = this.contents.findIndex(item => item.product.name === name);
    if (productIndex === -1) {
      console.log('Product not found in cart');
      return;
    }

    this.contents[productIndex].quantity = newQuantity;
    return this
  }

  // Clear the contents of the cart
  clearCartContents() {
    this.contents = [];
    this.totalAmount = 0;
    return this
  }

  // Compute the total amount of the cart
  computeTotal() {
    let total = 0;
    for (const item of this.contents) {
      total += item.product.price * item.quantity;
    }
    this.totalAmount = total;
    return this
  }

  // Check if the cart is empty
  isEmpty() {
    return this.contents.length === 0;
    return this
  }

  // Get the contents of the cart
  getContents() {
    return this.contents;
  }
}

// Define the Product class
class Product {
  constructor(name, price, isActive = true) {
    // Initialize a new product with a name, a price, and an active status
    this.name = name;
    this.price = price;
    this.isActive = isActive;
  }

  // Archive the product
  archive() {
    if (this.isActive) {
      this.isActive = false;
      return this
    }
  }

  unarchive() {
    if (!this.isActive) {
      this.isActive = true;
      return this
    }
  }

  // Update the price of the product
  updatePrice(newPrice) {
    this.price = newPrice;
    return this
  }
}